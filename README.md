# Interactive Web Application to Map Tumor Heterogeneity
The web application allows users to display interactive tree and migration graphs. Users can select files to display. A dropdown menu lists all files that can be displayed. The web application has a variety of features as described below.


## Dependencies
- - -
The web application is an exeutable HTML file with embedded JavaScript. The web application has the following dependencies:

* [JavaScript D3 (Data-Driven Documents) Version 3 library](https://d3js.org) to display the graphs
* [graphlib-dot library](https://github.com/dagrejs/graphlib-dot) to read imported DOT files
* [Google Palette library](https://github.com/google/palette.js/tree/master) to create a color palette to distinguish the various locations on the body where clones are found
* [Google Chrome Browser](https://www.google.com/chrome/browser/) to display the visualiztion correctly

The JavaScript D3 library, graphlib-dot library, and the Google Palette library dependencies are contained in the repository and imported in the HTML file. The Chrome Browser must be installed on the computer running the web application and used to open the HTML file.

## Installing
- - -
1. Download the repository
2. Open the HTML file named `graph_visualizaton_tool.html` in the [Google Chrome Browser](https://www.google.com/chrome/browser/) by copying the file path to the HTML file and into the search bar.

## Running
- - -

### Prerequisites
1. Files must be in DOT file format to be displayed on the web page. Examples of DOT files for tree and migration graphs are included in the repository under the names `exampleMigrationDOTFile.dot` and `exampleTreeDOTFile.dot`. Any type of file can be uploaded but only files of DOT file type will be listed in the dropdown menu.
2. Files must be in a format where a `T` denotes a tree graph, and a `G` or `GG` denotes a migration graph in order to display the correct graph.
3. The [Google Chrome Browser](https://www.google.com/chrome/browser/) must be installed to run the HTML file and display correctly.

### Graph Features

* **Stroke width of the tree edges corresponds to the number of mutations acquired**
* **Colors on the tree graph correspond to the locations on the migration graph**
* **Node labels on the migration graph describe location where cancer spread**
* **Node and edge labels on the tree graph can be displayed provided that the information is included in the DOT file**
* **The corresponding migration graph is displayed when a tree graph is selected**
* **Multigraph migration graphs can be displayed**

### Interactive Features

* **Selecting files to display**
	* Click the `Choose Files` button at the top left of the web page
	* Navigate through computer files, select the files to display, and click enter
	* The dropdown menu will list all files that can be displayed by the web application
* **Choose additional files to display after selecting files by clicking the `Choose Files` button**
	* Files with duplicate names will not be displayed; the first file selected with a certain name will be displayed
* **Multiple migration graphs can be displayed at once**
	* Select migration graphs on the dropdown menu
	* Multiple will be displayed
* **Remove all graphs on the screen by selecting the blank item on the top of the dropdown menu**
* **Highlighting the path from the root to the selected node**
	* Hover the cursor over the node
	* Remove cursor from over the node to remove highlight
* **Highlighting the subtree of a selected node**
	* Click on the node
	* Click on a different node to highlight a different subtree
	* Click the node again to remove the highlight
* **Displaying additional information about an edge/mutation**
	* Click on an edge to display a popup box with additional information
	* Click on the text displayed in the popup box to open a new tab with additional information
	* Click on a different edge to display a different popup box
	* Click on the edge again to close the popup box
* **Highlighting location of cells from migration graph on the tree graph**
	* Click on a node on the migration graph
	* Click on a different node on the migration graph to highlight nodes on the tree graph from a different location
	* Click on the node on the migration graph again to remove the highlight